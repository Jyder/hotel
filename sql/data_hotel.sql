-- OK
INSERT INTO Hotel.TypeChambre
  VALUES('Single',47,'Lit simple, Douche, Petit déjeuner');
INSERT INTO Hotel.TypeChambre
  VALUES('Double',65,'Grand lit, Baignoire, Petit déjeuner');
INSERT INTO Hotel.TypeChambre
  VALUES('Workable',92,'Grand lit, Baignoire, Bureau, Internet illimité, Petit déjeuner');
INSERT INTO Hotel.TypeChambre
  VALUES('Indisponible',0,'Rénovation en cours');
-- Erreur
INSERT INTO Hotel.Chambres VALUES(1,2,'Double');
-- OK
INSERT INTO Hotel.Chambres VALUES(1,1,'Single');
INSERT INTO Hotel.Chambres VALUES(1,2,'Single');
INSERT INTO Hotel.Chambres VALUES(1,3,'Indisponible');
INSERT INTO Hotel.Chambres VALUES(1,4,'Single');
INSERT INTO Hotel.Chambres VALUES(1,5,'Single');
INSERT INTO Hotel.Chambres VALUES(1,6,'Single');
INSERT INTO Hotel.Chambres VALUES(1,7,'Indisponible');
INSERT INTO Hotel.Chambres VALUES(1,8,'Single');
INSERT INTO Hotel.Chambres VALUES(1,9,'Workable');
INSERT INTO Hotel.Chambres VALUES(2,1,'Double');
INSERT INTO Hotel.Chambres VALUES(2,2,'Double');
INSERT INTO Hotel.Chambres VALUES(2,3,'Workable');
INSERT INTO Hotel.Chambres VALUES(2,4,'Double');
INSERT INTO Hotel.Chambres VALUES(2,5,'Double');
INSERT INTO Hotel.Chambres VALUES(2,6,'Workable');
INSERT INTO Hotel.Chambres VALUES(2,7,'Single');
INSERT INTO Hotel.Chambres VALUES(2,8,'Double');
INSERT INTO Hotel.Chambres VALUES(2,9,'Workable');
INSERT INTO Hotel.Chambres VALUES(3,1,'Workable');
INSERT INTO Hotel.Chambres VALUES(3,2,'Double');
INSERT INTO Hotel.Chambres VALUES(3,3,'Single');
INSERT INTO Hotel.Chambres VALUES(3,4,'Workable');
INSERT INTO Hotel.Chambres VALUES(3,5,'Double');
INSERT INTO Hotel.Chambres VALUES(3,6,'Double');
INSERT INTO Hotel.Chambres VALUES(3,7,'Single');
INSERT INTO Hotel.Chambres VALUES(3,8,'Double');
INSERT INTO Hotel.Chambres VALUES(3,9,'Double');
-- Erreurs
INSERT INTO Hotel.Chambres VALUES(2,26,'Double');
INSERT INTO Hotel.Chambres VALUES(7,1,'Double');
-- OK
INSERT INTO Hotel.PrixConsommation VALUES('Pepsi-Cola',2);
INSERT INTO Hotel.PrixConsommation VALUES('Téléphone',0.4);
INSERT INTO Hotel.PrixConsommation VALUES('Whisky-baby',9);
INSERT INTO Hotel.PrixConsommation VALUES('Sauna',11);

-- OK
INSERT INTO Hotel.Clients
  VALUES(DEFAULT,'Duchemin','Albert','10 rue des Jumeaux à Strasbourg');
INSERT INTO Hotel.Clients
  VALUES(DEFAULT,'Duchemin','Albert','13 rue de la Paix à Saillans (33141)');
INSERT INTO Hotel.Clients
  VALUES(DEFAULT,'Dupont','Avec un T','13 rue de la Paix à Saillans (33141)');
INSERT INTO Hotel.Clients
  VALUES(DEFAULT,'Dupond','Avec un D','13 rue de la Paix à Saillans (33141)');
INSERT INTO Hotel.Clients
  VALUES(DEFAULT,'Martin','Albert','13 rue de la Paix à Saillans (33141)');
INSERT INTO Hotel.Clients
  VALUES(DEFAULT,'Anonyme','Moi','13 rue de la Paix à Saillans (33141)');
-- Les attributs : Etage, Numero, DateDebut, DateFin, NumClient
-- OK
INSERT INTO Hotel.Reservations
  VALUES(1,2,'2017-02-26','2017-03-03',2);
INSERT INTO Hotel.Reservations
  VALUES(1,2,'2017-03-16','2017-03-21',2);
INSERT INTO Hotel.Reservations
  VALUES(1,2,'2017-04-16','2017-04-21',2);
INSERT INTO Hotel.Reservations
  VALUES(1,2,'2017-05-16','2017-05-21',2);
-- Erreur DateDebut > DateFin
INSERT INTO Hotel.Reservations
  VALUES(1,2,'2017-03-26','2017-03-23',2);
-- Erreur DateFin incorrecte car au milieu d'une réservation
INSERT INTO Hotel.Reservations
  VALUES(1,2,'2017-02-20','2017-03-01',2);
-- Erreur DateDebut incorrecte car au milieu d'une réservation
INSERT INTO Hotel.Reservations
  VALUES(1,2,'2017-02-28','2017-03-05',2);
-- Erreur (DateDebut,DateFin) encadre une réservation
INSERT INTO Hotel.Reservations
  VALUES(1,2,'2017-02-24','2017-03-05',2);
-- Erreur (DateDebut,DateFin) interieur à une réservation
INSERT INTO Hotel.Reservations
  VALUES(1,2,'2017-02-28','2017-03-01',2);
-- OK
INSERT INTO Hotel.Reservations
  VALUES(1,4,'2017-02-26','2017-03-03',3);
INSERT INTO Hotel.Reservations
  VALUES(2,1,'2017-03-16','2017-03-21',3);
INSERT INTO Hotel.Reservations
  VALUES(2,7,'2017-04-16','2017-04-21',3);
INSERT INTO Hotel.Reservations
  VALUES(3,2,'2017-05-16','2017-05-21',3);
