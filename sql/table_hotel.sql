CREATE SCHEMA Hotel;
SET search_path TO Hotel, public;
CREATE TABLE Hotel.TypeChambre (
  -- NOT NULL pour etre relationnel
  Categorie text NOT NULL, 
  Tarif integer NOT NULL CHECK (Tarif >= 0),
  Descriptif text NOT NULL,
  -- Clefs candidates
  PRIMARY KEY (Categorie)
);
CREATE TABLE Hotel.Chambres (
  -- NOT NULL pour etre relationnel
  Etage integer NOT NULL CHECK(Etage > 0),
  Numero integer NOT NULL CHECK(Numero > 0),
  Categorie text NOT NULL,
  -- Clefs candidates
  PRIMARY KEY (Etage, Numero),
  -- Clefs étrangères
  FOREIGN KEY (Categorie) REFERENCES Hotel.TypeChambre(Categorie)
  -- Contrainte integrite statique
  -- coherence (etage,numero) definie par une fonction
);
CREATE TABLE Hotel.PrixConsommation (
  -- NOT NULL pour etre relationnel
  Intitule text NOT NULL,
  PrixUnitaire real NOT NULL CHECK(PrixUnitaire > 0),
  -- Clefs candidates
  PRIMARY KEY (Intitule)
);
CREATE TABLE Hotel.Clients (
  -- NOT NULL pour etre relationnel
  NumClient serial NOT NULL,
  Nom text NOT NULL,
  Prenom text NOT NULL,
  Adresse text NOT NULL,
  -- Clefs candidates
  PRIMARY KEY (NumClient),
  UNIQUE (Nom, Prenom, Adresse)
);
CREATE TABLE Hotel.Consommations (
  -- NOT NULL pour etre relationnel
  NumClient serial NOT NULL,
  Jour date NOT NULL,
  Intitule text NOT NULL,
  Quantite integer NOT NULL CHECK (Quantite > 0),
  -- Clefs candidates
  PRIMARY KEY (NumClient, Jour, Intitule),
  -- Clefs étrangères
  FOREIGN KEY (NumClient) REFERENCES Hotel.Clients(NumClient),
  FOREIGN KEY (Intitule) REFERENCES Hotel.PrixConsommation(Intitule)
);
CREATE TABLE Hotel.Reservations (
  -- NOT NULL pour etre relationnel
  Etage integer NOT NULL,
  Numero integer NOT NULL,
  DateDebut date NOT NULL,
  DateFin date NOT NULL,
  NumClient serial NOT NULL,
  -- Clefs candidates
  PRIMARY KEY (Etage, Numero, DateDebut),
  UNIQUE (Etage, Numero, DateFin),
  -- Clefs externes
  FOREIGN KEY (NumClient) REFERENCES Hotel.Clients(NumClient),
  FOREIGN KEY (Etage, Numero) REFERENCES Hotel.Chambres(Etage, Numero),
  -- Contrainte intégrité élémentaire
  CHECK (DateDebut < DateFin)
  -- Contrainte intégrité statique
  -- cohérence (Etage, Numero, DateDebut, DateFin) definie par une fonction
);
