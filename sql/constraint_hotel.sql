-- PostgreSQL n'accepte pas les contraintes d'integrite avec un SELECT
-- Il faut definir une fonction
CREATE FUNCTION Hotel.DatesDebutFinCorrectes(integer, integer, date, date) 
  RETURNS boolean AS $$ 
SELECT NOT EXISTS (
  SELECT * 
  FROM Hotel.Reservations
  WHERE $1 = Etage
    AND $2 = Numero 
    AND $3 <= DateFin
    AND $4 >= DateDebut)
$$ LANGUAGE SQL;
-- puis appeler la fonction dans une contrainte d'integrite
ALTER TABLE Hotel.Reservations ADD CONSTRAINT DatesPossibles
  CHECK(Hotel.DatesDebutFinCorrectes(Etage, Numero, DateDebut, DateFin));
-- PostgreSQL n'accepte pas les contraintes d'integrite avec un SELECT
-- Il faut definir une fonction
-- L'unicite n'est pas testée ici car présente dans la clef
CREATE FUNCTION Hotel.EtageNumeroCorrects(integer, integer) 
  RETURNS boolean AS $$ 
SELECT ($1 = 1 AND $2 = 1)
  OR EXISTS (
  SELECT * 
  FROM Hotel.Chambres
  WHERE ($1 = Etage AND $2 = (Numero + 1))
     OR ($1 = (Etage + 1) AND $2 = 1))
$$ LANGUAGE SQL;
-- puis appeler la fonction dans une contrainte d'integrite
ALTER TABLE Hotel.Chambres ADD CONSTRAINT NumeroPossible
  CHECK(Hotel.EtageNumeroCorrects(Etage, Numero));
